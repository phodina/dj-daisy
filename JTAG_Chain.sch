EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "FM Board JTAG Adapter"
Date "2018-11-28"
Rev "1.0"
Comp "SIEMENS"
Comment1 "Petr Hodina"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:CoreSight_JTAG_SWD_20 J1
U 1 1 5C01B7A1
P 2200 4150
F 0 "J1" H 1670 4196 50  0000 R CNN
F 1 "Channel_A" H 1670 4105 50  0000 R CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_2x10_P1.27mm_Vertical" H 2650 3100 50  0001 L TNN
F 3 "http://infocenter.arm.com/help/topic/com.arm.doc.dui0499b/DUI0499B_system_design_reference.pdf" V 1850 2900 50  0001 C CNN
	1    2200 4150
	1    0    0    -1  
$EndComp
$Comp
L Connector:CoreSight_JTAG_SWD_20 J3
U 1 1 5C01B823
P 5250 4150
F 0 "J3" H 4720 4196 50  0000 R CNN
F 1 "Debugger" H 4720 4105 50  0000 R CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_2x10_P1.27mm_Vertical" H 5700 3100 50  0001 L TNN
F 3 "http://infocenter.arm.com/help/topic/com.arm.doc.dui0499b/DUI0499B_system_design_reference.pdf" V 4900 2900 50  0001 C CNN
	1    5250 4150
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x05_Male J2
U 1 1 5C01B9F2
P 3100 4550
F 0 "J2" H 3072 4480 50  0000 R CNN
F 1 "TRACE_A" H 3072 4571 50  0000 R CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x05_P1.27mm_Vertical_SMD_Pin1Left" H 3100 4550 50  0001 C CNN
F 3 "~" H 3100 4550 50  0001 C CNN
	1    3100 4550
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x05_Male J4
U 1 1 5C01BB02
P 6150 4550
F 0 "J4" H 6123 4480 50  0000 R CNN
F 1 "TRACE_DBG" H 6123 4571 50  0000 R CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x05_P1.27mm_Vertical_SMD_Pin1Left" H 6150 4550 50  0001 C CNN
F 3 "~" H 6150 4550 50  0001 C CNN
	1    6150 4550
	-1   0    0    1   
$EndComp
Wire Wire Line
	5850 4350 5950 4350
Wire Wire Line
	5950 4450 5850 4450
Wire Wire Line
	5850 4550 5950 4550
Wire Wire Line
	5950 4650 5850 4650
Wire Wire Line
	5850 4750 5950 4750
Wire Wire Line
	2800 4750 2900 4750
Wire Wire Line
	2900 4650 2800 4650
Wire Wire Line
	2800 4550 2900 4550
Wire Wire Line
	2900 4450 2800 4450
Wire Wire Line
	2800 4350 2900 4350
Wire Wire Line
	2800 4150 3100 4150
Wire Wire Line
	9100 4200 9400 4200
Wire Wire Line
	9100 4100 9400 4100
Wire Wire Line
	9100 4000 9400 4000
Wire Wire Line
	9100 3700 9400 3700
Wire Wire Line
	5850 3750 6150 3750
Wire Wire Line
	6150 3650 5850 3650
Wire Wire Line
	5850 3950 6150 3950
Wire Wire Line
	6150 4050 5850 4050
Wire Wire Line
	5850 4150 6150 4150
Wire Wire Line
	2800 4050 3100 4050
Wire Wire Line
	3100 3950 2800 3950
Wire Wire Line
	2800 3750 3100 3750
Wire Wire Line
	2800 3650 3100 3650
$Comp
L JTAG_Chain-rescue:GND-power #PWR01
U 1 1 5C0331F0
P 2200 5050
F 0 "#PWR01" H 2200 4800 50  0001 C CNN
F 1 "GND" H 2205 4877 50  0000 C CNN
F 2 "" H 2200 5050 50  0001 C CNN
F 3 "" H 2200 5050 50  0001 C CNN
	1    2200 5050
	1    0    0    -1  
$EndComp
$Comp
L JTAG_Chain-rescue:GND-power #PWR02
U 1 1 5C03322C
P 5250 5050
F 0 "#PWR02" H 5250 4800 50  0001 C CNN
F 1 "GND" H 5255 4877 50  0000 C CNN
F 2 "" H 5250 5050 50  0001 C CNN
F 3 "" H 5250 5050 50  0001 C CNN
	1    5250 5050
	1    0    0    -1  
$EndComp
$Comp
L JTAG_Chain-rescue:GND-power #PWR03
U 1 1 5C03326F
P 8500 5100
F 0 "#PWR03" H 8500 4850 50  0001 C CNN
F 1 "GND" H 8505 4927 50  0000 C CNN
F 2 "" H 8500 5100 50  0001 C CNN
F 3 "" H 8500 5100 50  0001 C CNN
	1    8500 5100
	1    0    0    -1  
$EndComp
Text Label 9250 3700 0    50   ~ 0
TMS
Text Label 9250 4000 0    50   ~ 0
TDO_B
Text Label 9250 4100 0    50   ~ 0
TDO_A
Text Label 2950 3650 0    50   ~ 0
TMS
Text Label 2950 3750 0    50   ~ 0
TCK
Text Label 2950 3950 0    50   ~ 0
TDO_A
Text Label 2950 4050 0    50   ~ 0
TDI_DBG
Text Label 6000 3650 0    50   ~ 0
TMS
Text Label 6000 3750 0    50   ~ 0
TCK
Text Label 6000 3950 0    50   ~ 0
TDO_B
Text Label 6000 4050 0    50   ~ 0
TDI_DBG
Text Label 6000 4150 0    50   ~ 0
SRST
$Comp
L JTAG_Chain-rescue:D_Schottky-Device D1
U 1 1 5C03B1E7
P 6550 2700
F 0 "D1" H 6550 2916 50  0000 C CNN
F 1 "D_Schottky" H 6550 2825 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323" H 6550 2700 50  0001 C CNN
F 3 "~" H 6550 2700 50  0001 C CNN
	1    6550 2700
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J5
U 1 1 5C03FC80
P 7050 2800
F 0 "J5" H 7022 2680 50  0000 R CNN
F 1 "SRST" H 7022 2771 50  0000 R CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x02_P1.27mm_Vertical" H 7050 2800 50  0001 C CNN
F 3 "~" H 7050 2800 50  0001 C CNN
	1    7050 2800
	-1   0    0    1   
$EndComp
Text Label 6050 2800 0    50   ~ 0
SRST
$Comp
L Connector:Conn_01x03_Male J6
U 1 1 5C049B14
P 7050 3200
F 0 "J6" H 7022 3130 50  0000 R CNN
F 1 "VCC" H 7022 3221 50  0000 R CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x03_P1.27mm_Vertical" H 7050 3200 50  0001 C CNN
F 3 "~" H 7050 3200 50  0001 C CNN
	1    7050 3200
	-1   0    0    1   
$EndComp
Text Label 8500 2950 3    50   ~ 0
VCC_CH_B
Text Label 5250 2950 3    50   ~ 0
VCC_DBG
Text Label 2200 2950 3    50   ~ 0
VCC_CH_A
Wire Wire Line
	2200 2950 2200 3350
Wire Wire Line
	5250 2950 5250 3350
Wire Wire Line
	8500 2950 8500 3400
Wire Wire Line
	6850 3100 6350 3100
Wire Wire Line
	6850 3200 6350 3200
Wire Wire Line
	6850 3300 6350 3300
Text Label 6350 3100 0    50   ~ 0
VCC_CH_A
Text Label 6350 3300 0    50   ~ 0
VCC_CH_B
Text Label 6350 3200 0    50   ~ 0
VCC_DBG
Wire Wire Line
	9100 3800 9400 3800
Text Label 9250 3800 0    50   ~ 0
TCK
Wire Wire Line
	6850 2700 6700 2700
Wire Wire Line
	6400 2700 6350 2700
Wire Wire Line
	6350 2700 6350 2800
Wire Wire Line
	6350 2800 6850 2800
Wire Wire Line
	6350 2800 6050 2800
Connection ~ 6350 2800
$Comp
L Connector:Conn_01x05_Male J8
U 1 1 5C06382C
P 9400 4600
F 0 "J8" H 9373 4530 50  0000 R CNN
F 1 "TRACE_B" H 9373 4621 50  0000 R CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x05_P1.27mm_Vertical_SMD_Pin1Left" H 9400 4600 50  0001 C CNN
F 3 "~" H 9400 4600 50  0001 C CNN
	1    9400 4600
	-1   0    0    1   
$EndComp
Wire Wire Line
	9200 4400 9100 4400
Wire Wire Line
	9100 4500 9200 4500
Wire Wire Line
	9200 4600 9100 4600
Wire Wire Line
	9100 4700 9200 4700
Wire Wire Line
	9200 4800 9100 4800
$Comp
L Connector:CoreSight_JTAG_SWD_20 J7
U 1 1 5C01B88A
P 8500 4200
F 0 "J7" H 7970 4246 50  0000 R CNN
F 1 "Channel_B" H 7970 4155 50  0000 R CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_2x10_P1.27mm_Vertical" H 8950 3150 50  0001 L TNN
F 3 "http://infocenter.arm.com/help/topic/com.arm.doc.dui0499b/DUI0499B_system_design_reference.pdf" V 8150 2950 50  0001 C CNN
	1    8500 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 5000 8500 5100
Wire Wire Line
	5250 4950 5250 5050
Wire Wire Line
	2200 4950 2200 5050
NoConn ~ 3100 4150
NoConn ~ 9400 4200
$Comp
L Connector:Conn_01x04_Male J9
U 1 1 5C077B4E
P 7050 2050
F 0 "J9" H 7023 1930 50  0000 R CNN
F 1 "Conn_01x04_Male" H 7023 2021 50  0000 R CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x04_P1.27mm_Vertical_SMD_Pin1Left" H 7050 2050 50  0001 C CNN
F 3 "~" H 7050 2050 50  0001 C CNN
	1    7050 2050
	-1   0    0    1   
$EndComp
Wire Wire Line
	6850 1850 6800 1850
Wire Wire Line
	6800 1850 6800 1950
Wire Wire Line
	6800 2150 6850 2150
Wire Wire Line
	6850 2050 6800 2050
Connection ~ 6800 2050
Wire Wire Line
	6800 2050 6800 2150
Wire Wire Line
	6850 1950 6800 1950
Connection ~ 6800 1950
Wire Wire Line
	6800 1950 6800 2050
$Comp
L JTAG_Chain-rescue:GND-power #PWR0101
U 1 1 5C07A446
P 6800 2200
F 0 "#PWR0101" H 6800 1950 50  0001 C CNN
F 1 "GND" H 6805 2027 50  0000 C CNN
F 2 "" H 6800 2200 50  0001 C CNN
F 3 "" H 6800 2200 50  0001 C CNN
	1    6800 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 2200 6800 2150
Connection ~ 6800 2150
$Comp
L JTAG_Chain-rescue:LED-Device D2
U 1 1 5C07B3AC
P 4100 2100
F 0 "D2" V 4138 1983 50  0000 R CNN
F 1 "PWR_A" V 4047 1983 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 4100 2100 50  0001 C CNN
F 3 "~" H 4100 2100 50  0001 C CNN
	1    4100 2100
	0    -1   -1   0   
$EndComp
$Comp
L JTAG_Chain-rescue:LED-Device D3
U 1 1 5C07B56E
P 4550 2100
F 0 "D3" V 4588 1982 50  0000 R CNN
F 1 "PWR_B" V 4497 1982 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 4550 2100 50  0001 C CNN
F 3 "~" H 4550 2100 50  0001 C CNN
	1    4550 2100
	0    -1   -1   0   
$EndComp
$Comp
L JTAG_Chain-rescue:R-Device R1
U 1 1 5C07B5DC
P 4100 2500
F 0 "R1" H 4170 2546 50  0000 L CNN
F 1 "R" H 4170 2455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4030 2500 50  0001 C CNN
F 3 "~" H 4100 2500 50  0001 C CNN
	1    4100 2500
	1    0    0    -1  
$EndComp
$Comp
L JTAG_Chain-rescue:R-Device R2
U 1 1 5C07B647
P 4550 2500
F 0 "R2" H 4620 2546 50  0000 L CNN
F 1 "R" H 4620 2455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4480 2500 50  0001 C CNN
F 3 "~" H 4550 2500 50  0001 C CNN
	1    4550 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 2650 4550 2700
Wire Wire Line
	4550 2700 4350 2700
Wire Wire Line
	4100 2700 4100 2650
$Comp
L JTAG_Chain-rescue:GND-power #PWR0102
U 1 1 5C07C61A
P 4350 2800
F 0 "#PWR0102" H 4350 2550 50  0001 C CNN
F 1 "GND" H 4355 2627 50  0000 C CNN
F 2 "" H 4350 2800 50  0001 C CNN
F 3 "" H 4350 2800 50  0001 C CNN
	1    4350 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 2800 4350 2700
Connection ~ 4350 2700
Wire Wire Line
	4350 2700 4100 2700
Wire Wire Line
	4550 2350 4550 2250
Wire Wire Line
	4100 2250 4100 2350
Wire Wire Line
	4100 1950 4100 1300
Wire Wire Line
	4550 1950 4550 1300
Text Label 4100 1300 3    50   ~ 0
VCC_CH_A
Text Label 4550 1300 3    50   ~ 0
VCC_CH_B
$Comp
L JTAG_Chain-rescue:MountingHole_Pad-Mechanical MH1
U 1 1 5C082200
P 5500 1500
F 0 "MH1" H 5600 1551 50  0000 L CNN
F 1 " " H 5600 1460 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm" H 5500 1500 50  0001 C CNN
F 3 "~" H 5500 1500 50  0001 C CNN
	1    5500 1500
	1    0    0    -1  
$EndComp
$EndSCHEMATC
