# DJ Daisy board

Debug JTAG Daisy board is aimed to debug two ARM cortex-m cores. This eases the development and debugging by concurrent running application.

# BOM

To order the components for the board from Farnell please see [BOM](bom_farnell.txt)

# Schematics

![schematics](img/schematics.png)

# Board

![board](img/board.png)

# Visualization

![3d](img/3d_view.png)

# Manufacturing

Manufacturing files (*Gerber* and *Excellon*) are in directory **Manufacture**.

## Tools

To open the project download *[Kicad 5.0.0](http://kicad-pcb.org/download/)*

# Authors

- [Petr Hodina](https://www.linkedin.com/in/petr-hodina/)
- [Tomas Galbička](https://www.linkedin.com/in/tom%C3%A1%C5%A1-galbi%C4%8Dka-6b693340/)
- [Charles Bajeux](https://www.linkedin.com/in/charles-bajeux-857ab11b)

# Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

# License
[MIT](License)
